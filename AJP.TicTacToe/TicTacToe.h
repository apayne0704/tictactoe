#pragma once

#include <iostream>
#include <conio.h>

class TicTacToe
{
private:
	//Fields
	char m_board[10]; //An array used to store the spaces of the game board. Once a player has Moved in one of the positions, the space should change to an X or an O.
	int m_numTurns; //This will be used to detect when the board is full (all nine spaces are taken).
	char m_playerTurn; // This is used to track if it's X's turn or O's turn.
	char m_winner; //This is used to check to see the winner of the game. A space should be used while the game is being played.

public:
	// Constructor
	TicTacToe();
	// Accessor
	char GetPlayerTurn();
	// Other Methods
	void DisplayBoard();
	bool IsOver();
	bool IsValidMove(int position);
	void Move(int position);
	void DisplayResult();
	void CheckWinCondition(int position1, int position2, int position3);
};