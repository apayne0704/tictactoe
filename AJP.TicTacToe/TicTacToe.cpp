#include "TicTacToe.h"

//Constructor
TicTacToe::TicTacToe() {
	// No parameters, initializes all of the fields including the game board
	for (int i = 0; i < 9; i++) m_board[i] = ' ';
	m_board[9] = '\0';
	m_numTurns = 1;
	m_playerTurn = 'X';
	m_winner = ' ';
}

// Accessors (gets)
char TicTacToe::GetPlayerTurn() {
	// Returns an 'X' or an 'O,' depending on which player's turn it is.
	return m_playerTurn;
}

// Other methods
void TicTacToe::DisplayBoard() {
	// This method will output the board to the console.
	std::cout << m_board[0] << "|" << m_board[1] << "|" << m_board[2] << "\n"
		<< m_board[3] << "|" << m_board[4] << "|" << m_board[5] << "\n"
		<< m_board[6] << "|" << m_board[7] << "|" << m_board[8] << "\n";
}

bool TicTacToe::IsOver() {
	// Returns true if the game is over (winner or tie), otherwise false if the game is still being played.
	if (m_winner == ' ') return false;
	else return true;
}

bool TicTacToe::IsValidMove(int position) {
	// Returns true if the space on the board is open, otherwise false.
	if (m_board[position] == ' ') return true;
	else return false;
}

void TicTacToe::Move(int position) {
	//Param: int (1 - 9) representing a square on the board. Places the current players character in the specified position on the board.
	m_board[position] = m_playerTurn;

	if (m_numTurns <= 9) {
		//Pass into the CheckWinCondition function the 8 possible win scenarios for Tic Tac Toe
		// Horizontal
		CheckWinCondition(0, 1, 2);
		CheckWinCondition(3, 4, 5);
		CheckWinCondition(6, 7, 8);
		// Vertical
		CheckWinCondition(0, 3, 6);
		CheckWinCondition(1, 4, 7);
		CheckWinCondition(2, 5, 8);
		// Diagonal
		CheckWinCondition(0, 4, 8);
		CheckWinCondition(2, 4, 6);

		//On the 9th Turn, if no winner was set, set a tie
		if (m_numTurns == 9 && m_winner == ' ')  m_winner = 'T'; 
	}

	// If there is no winner, change players turn and increment number of turns
	if (m_winner == ' ') {
		if (m_playerTurn == 'X') m_playerTurn = 'O';
		else m_playerTurn = 'X';
		m_numTurns++;
	}
}

void TicTacToe::DisplayResult() {
	//Displays a message stating who the winning player is, or if the game ended in a tie.
	if (m_winner == 'T') {
		std::cout << "The Game was a Tie.\n";
	}
	else {
		std::cout << "The winner is: " << m_playerTurn << "\n";

	}
}

	void TicTacToe::CheckWinCondition(int position1, int position2, int position3) {
		// Check three passed positions; if all are X or all are O, set winner
		switch (m_board[position1] | m_board[position2] | m_board[position3]) {
		case 'X' | 'X' | 'X':
			m_winner = 'X';
			break;
		case 'O' | 'O' | 'O':
			m_winner = 'O';
			break;
		default:
			break;
		}

	}